<?php
/*
Plugin Name: Ilkotech Integration
Plugin URI: https://ilkotech.co.uk
Description: Adds support for logging into Wordpress with Ilkotech
Version: 2.2
Author: Ilkotech LTD
Author URI: https://ilkotech.co.uk
License: GPL2
*/
/*
 = 2.1 =
 Fixed redirect loop, mistakenly getting the first (oldest) token from the table for a user rather than the last (latest) code.
*/
// Now with added OAuth2!

$myURL = get_option('siteurl');
function redirect_uri() {
	return esc_attr( get_option('siteurl') )."/wp-json/ilkotech/v2/auth";
}

// Config notices
function sample_admin_notice__error() {
	$class = 'notice notice-error';
	$message = __( '<strong>Hey!</strong><br/>You\'ve not set an API key for Ilkotech yet.<br/>Please set it to enable functionality!', 'sample-text-domain' );
	printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), $message ); 
}
// Replace page
function goto_login_page() {
	// Basic init
	global $redirect_uri;
	
	if (!get_option('ilkotech_app_secret')) {
		add_action( 'admin_notices', 'sample_admin_notice__error' );
		return;
	}
	
	$page = parse_url(basename($_SERVER['REQUEST_URI']));
	if (isset($page["query"])) {
		parse_str($page["query"],$page["query"]);
	}
	
	if( $page['path'] == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {
		if (!isset($page["query"]["action"])) {
			$newPath = 'https://accounts.ilkotech.co.uk/oauth/authorize?response_type=code&response=web&redirect_uri='.urlencode(redirect_uri()).'&client_id='.get_option('ilkotech_app_public');
			wp_redirect($newPath);
		} else if (strtolower($page["query"]["action"]) == "logout") {
			wp_logout();
			wp_redirect("/");
			exit;
		}
	}
	
	// Update current user.
	if (is_user_logged_in()) {
		$user = wp_get_current_user();
		$hash = get_user_hash($user->data->ID);
		if ($hash != null) {
			$remote = get_remote_user($hash);
			if ($remote == null) {
				wp_logout();
				wp_redirect("/wp-login.php");
				exit;
			}
			wp_update_user( array( 'ID' => $user->data->ID, 'user_email' => $remote["email"], 'username' => $remote["username"], 'display_name' => $remote["name"], 'nickname' => $remote["name"] ) );
			update_user_meta($user->data->ID, 'nickname', $remote["name"]);
		}
	}
}
add_action('init','goto_login_page');

// Disable profile page
add_action ('init' , 'wpse_redirect_profile_access');

function sql_check(){
	global $wpdb;
	
	$table_name = $wpdb->prefix.'ilkotech_tokens';
	if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
		//table not in database. Create new table
		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE `$table_name` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`auth_code` longtext,
			`access_token` longtext,
			`refresh_token` longtext,
			`expires_in` int(11) DEFAULT NULL,
			`user_hash` longtext,
			PRIMARY KEY (`id`)
			) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
		";
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
	}
	
	$table_name = $wpdb->prefix.'ilkotech_users';
	if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
		//table not in database. Create new table
		$charset_collate = $wpdb->get_charset_collate();

		$sql = "
			CREATE TABLE `$table_name` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `wordpress_id` int(11) DEFAULT NULL,
			  `ilkotech_id` text,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
		";
		
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
	}
}
register_activation_hook( __FILE__, 'sql_check' );

function wpse_redirect_profile_access() {
	if (!get_option('ilkotech_app_secret')) return;
	
	//admin won't be affected
	if (current_user_can('manage_options')) return '';

	//if we're at admin profile.php page
	if (strpos ($_SERVER ['REQUEST_URI'] , 'wp-admin/profile.php' )) {
		wp_redirect("https://accounts.ilkotech.co.uk/"); // to page like: example.com/my-profile/
		exit();
	}
}


// API for auth
add_action( 'rest_api_init', function () {
	// Register our API routes
	register_rest_route( 'ilkotech/v2', '/auth/', array('methods' => 'GET','callback' => 'handle_auth',));
} );

// Response from Ilkotech!
function handle_auth($data) {
	
	global $wpdb;
	
	// Get the query parameters.
	$params = $data->get_params();

	// Got code?
	if (!isset($params['code'])) return "Missing code!";
	
	// Exchange the code for the access token.
	$url = "https://accounts.ilkotech.co.uk/oauth/token";
	
	$data = [
		'grant_type' => 'authorization_code',
		'client_id' => get_option('ilkotech_app_public'),
		'client_secret' => get_option('ilkotech_app_secret'),
		'redirect_uri' => redirect_uri(),
		'code' => $params['code']
	];
	//return $data;
	$response = wp_remote_post($url,["body" => $data]);
	$response = json_decode($response["body"],true);
	
	if ($response['message']) {
		die("Error: ".$response['message']);
		return $response;
	}
	
	// Insert into db
	$data = [
		'auth_code' => $params['code'],
		'access_token' => $response['access_token'],
		'refresh_token' => $response['refresh_token'],
		'expires_in' => $response['expires_in'],
		'user_hash' => $response['uid']
	];
	$r = $wpdb->insert($wpdb->prefix."ilkotech_tokens",$data);
	if (!$r) {
		echo "Error writing to database #1";
		die($r);
		return "ERR";
	}
	
	// Do we know the user?
	$user = get_stored_user($response['uid']);
	
	if ($user == null) {
		
		// Create them.
		$ruser = $response['info'];
		
		if ($ruser == null) return "Unknown Ilkotech User!";
		var_dump($ruser);
		
		$new_user = wp_create_user( $ruser["username"], "none", $ruser["email"] );
		
		if( is_wp_error( $new_user ) ) {
			return $new_user->get_error_message();
		}
		
		$user = get_user_by( 'id' , $new_user );
		
		// Add them to our db.
		
		$r = $wpdb->insert($wpdb->prefix."ilkotech_users",["ilkotech_id"=>$response['uid'],"wordpress_id"=>$new_user]);
		if (!$r) {
			echo "Error writing to database #2";
			die($r);
			return "ERR";
		}
		
		// Update their info
		update_user_meta($user->data->ID, 'nickname', $ruser["name"]);
		
	} else {
		
		// Update them.
		
		$user_id = wp_update_user( array( 'ID' => $user->data->ID, 'user_email' => $response['info']["email"], 'username' => $response['info']["username"], 'display_name' => $response['info']["name"] ) );
		
		update_user_meta($user->data->ID, 'nickname', $response['info']["name"]);
		
	}
	
	// Log them in.
	wp_set_current_user( $user->data->ID, $user->user_login );
	wp_set_auth_cookie( $user->data->ID );
	do_action( 'wp_login', $user->user_login );
	
	// Send em home!
	wp_redirect("/");
	exit;
}

add_action('manage_users_columns','modify_user_columns');
function modify_user_columns($column_headers) {
  $column_headers['ilkotech_linked'] = 'Linked to Ilkotech';
  return $column_headers;
}

add_action('manage_users_custom_column', 'kjl_user_posts_count_column_content', 10, 3);
function kjl_user_posts_count_column_content($value, $column_name, $user_id) {
	if ( 'ilkotech_linked' == $column_name ) {
		$hash = get_user_hash($user_id);
		if ($hash) return "<strong>Yes</strong>";
		return '<strong>No</strong>';
	}
	return $value;
}

function get_stored_auth($hash,$secret=true) {
	// Attempts to get a stored auth attempt, null on failiure.
	global $wpdb;
	
	if ($secret) {
		$auth = $wpdb->get_row( "SELECT * FROM ".$wpdb->prefix."ilkotech WHERE `auth_secret` = '".$hash."'" );
		return $auth;
	} else {
		$auth = $wpdb->get_row( "SELECT * FROM ".$wpdb->prefix."ilkotech WHERE `auth_public` = '".$hash."'" );
		return $auth;
	}
	
}

// Gets WP user based on Ilkotech Hash
function get_stored_user($hash) {
	//$user = get_user_by( 'id' , $id );
	global $wpdb;
	
	$linked_acc = $wpdb->get_row( "SELECT * FROM ".$wpdb->prefix."ilkotech_users WHERE `ilkotech_id` = '".$hash."'" );
	if ($linked_acc == null) return null;
	
	$u = get_user_by( 'id' , $linked_acc->wordpress_id );
	
	return $u;
}
function get_user_hash($uid) {
	global $wpdb;
	
	$linked_acc = $wpdb->get_row( "SELECT * FROM ".$wpdb->prefix."ilkotech_users WHERE `wordpress_id` = '".$uid."'" );
	if ($linked_acc == null) return null;
	
	return $linked_acc->ilkotech_id;
}
// Downloads a user from Ilkotechs API
// Null on failiure.

// DEPRECATED
function get_remote_user($hash) {
	$tokens = get_tokens($hash);
	
	if ($tokens == null) return null;
	
	$url = "https://accounts.ilkotech.co.uk/api/v2/user";
	
	$response = wp_remote_get($url,["headers"=> [ "Authorization" => "Bearer ".$tokens->access_token ] ]);
	$response = json_decode($response["body"],true);

	if ($response['error']) {
		return null;
	}
	return $response;
}

// Gets the latest tokens for the associated user
function get_tokens($hash) {
	global $wpdb;
	
	$linked_acc = $wpdb->get_row( "SELECT * FROM ".$wpdb->prefix."ilkotech_tokens WHERE `user_hash` = '".$hash."' ORDER BY id DESC" );
	if ($linked_acc == null) return null;
	
	return $linked_acc;
}
// Avatars

function my_custom_avatar( $avatar, $id_or_email, $size, $default, $alt ) {
    $user = false;

    if ( is_numeric( $id_or_email ) ) {

        $id = (int) $id_or_email;
        $user = get_user_by( 'id' , $id );

    } elseif ( is_object( $id_or_email ) ) {

        if ( ! empty( $id_or_email->user_id ) ) {
            $id = (int) $id_or_email->user_id;
            $user = get_user_by( 'id' , $id );
        }

    } else {
        $user = get_user_by( 'email', $id_or_email );	
    }

    if ( $user && is_object( $user ) ) {

        //if ( $user->data->ID == '1' ) {
            $avatar = 'https://avatar.ilkotech.co.uk/user/'.$user->user_login.'/'.$size;
            $avatar = "<img alt='{$alt}' src='{$avatar}' class='avatar avatar-{$size} photo' height='{$size}' width='{$size}' />";
        //}

    }

    return $avatar;
}

if ( get_option( 'ilkotech_avatars' , 1) ) {
	add_filter( 'avatar_defaults', 'new_default_avatar' );
	add_filter( 'get_avatar' , 'my_custom_avatar' , 1 , 5 );
}
function new_default_avatar ( $avatar_defaults ) {
		//Set the URL where the image file for your avatar is located
		$new_avatar_url = 'https://avatar.ilkotech.co.uk/';
		//Set the text that will appear to the right of your avatar in Settings>>Discussion
		$avatar_defaults[$new_avatar_url] = 'Ilkotech Avatars';
		return $avatar_defaults;
}

// Set default scheme
function set_default_admin_color($user_id) {
    $args = array(
        'ID' => $user_id,
        'admin_color' => 'midnight'
    );
    wp_update_user( $args );
}
add_action('user_register', 'set_default_admin_color');

// create custom plugin settings menu
add_action('admin_menu', 'my_cool_plugin_create_menu');

function my_cool_plugin_create_menu() {

	//create new top-level menu
	add_menu_page('Ilkotech | Integration Settings', 'Ilkotech', 'administrator', __FILE__, 'ilkotech_options_page');

	//call register settings function
	add_action( 'admin_init', 'register_my_cool_plugin_settings' );
}


function register_my_cool_plugin_settings() {
	//register our settings
	register_setting( 'ilkotech_options_group', 'ilkotech_app_secret' );
	register_setting( 'ilkotech_options_group', 'ilkotech_app_public' );
	register_setting( 'ilkotech_options_group', 'ilkotech_avatars' );
}

function ilkotech_options_page() {
	$cb = esc_attr( get_option('siteurl') )."/wp-json/ilkotech/v1/login";
?>
<div class="wrap">
<h1>Ilkotech Integration - v2 OAuth</h1>
Here you can alter how Ilkotechs affects your WordPress blog.
<br/>
Account integration is a base function you cannot turn off.
<br/>
You can find your Applications keys in the Ilkotech Account Center.

<form method="post" action="options.php">
    <?php settings_fields( 'ilkotech_options_group' ); ?>
    <?php do_settings_sections( 'ilkotech_options_group' ); ?>
    <table class="form-table">
		<tr valign="top">
        <th scope="row">Client ID</th>
        <td><input type="text" name="ilkotech_app_public" size="50" value="<?php echo esc_attr( get_option('ilkotech_app_public') ); ?>" /></td>
        </tr>
		
		<tr valign="top">
        <th scope="row">Client Secret</th>
        <td><input type="text" name="ilkotech_app_secret" size="50" value="<?php echo esc_attr( get_option('ilkotech_app_secret') ); ?>" /></td>
        </tr>
		
		<tr valign="top">
			<th scope="row">Optional Features</th>
			<td>
				<fieldset>
					<legend class="screen-reader-text"><span>Toolbar</span></legend>
					<label for="ilkotech_avatars">
						<input name="ilkotech_avatars" type="checkbox" value="1" <?php checked( '1', get_option( 'ilkotech_avatars' ) ); ?>>
						Replace Avatars with Ilkotech Avatars
					</label>
					<br>
				</fieldset>
			</td>
        </tr>
		
    </table>
    
    <?php submit_button(); ?>

</form>
</div>
<?php } ?>